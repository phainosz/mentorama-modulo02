import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Serializacao {
    public static void main(String[] args) {
        List<String> dados = Arrays.asList("Numero", "Nome", "Cidade", "Telefone");

        try (ObjectOutputStream oos = new ObjectOutputStream(Files.newOutputStream(Paths.get("nomes.ser")))) {
            oos.writeObject(dados);
            dados = null;
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Deserializacao");

        try (ObjectInputStream ois = new ObjectInputStream(Files.newInputStream(Paths.get("nomes.ser")))) {
            dados = (List<String>) ois.readObject();
            System.out.println(dados);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
