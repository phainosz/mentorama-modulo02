import java.util.Scanner;

class Principal {
  public static void main(String[] args) {
    int valor = 0;
    Scanner scanner = new Scanner(System.in);
    while (valor != 3) {
      System.out.println("--------------");
      System.out.println("MENU DE OPÇÕES:");
      System.out.println("1- Opção 1");
      System.out.println("2- Opção 2");
      System.out.println("3- Opção 3");
      System.out.println("Digite a opção desejada:");
      System.out.println("--------------");

      valor = scanner.nextInt();

      switch (valor) {
      case 1:
        System.out.println("Você escolheu a primeira opção");
        break;
      case 2:
        System.out.println("Você escolheu a segunda opção");
        break;
      case 3:
        System.out.println("O programa foi encerrado");
        break;
      default:
        System.out.println("Opção inválida!");
        break;
      }
    }
    scanner.close();
  }
}