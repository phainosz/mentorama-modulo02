import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JavaIO {
  public static void main(String... args) throws IOException {

    File arquivo = new File("Arquivo.txt");
    if (!arquivo.exists()) {
      arquivo.createNewFile();
    }

    try (BufferedWriter bw = Files.newBufferedWriter(Paths.get("Arquivo.txt"));
        BufferedReader br = Files.newBufferedReader(Paths.get("Arquivo.txt"))) {

      bw.write("Primeira linha");
      bw.newLine();
      bw.write("Segunda linha");
      bw.newLine();
      bw.flush();

      String text = null;

      while ((text = br.readLine()) != null) {
        System.out.println(text);
      }

    } catch (IOException e) {
      e.printStackTrace();
    }

    Path path = Paths.get("Arquivo2.txt");
    if (!Files.exists(path)) {
      Files.createFile(path);
    }

    try (BufferedInputStream bis = new BufferedInputStream(Files.newInputStream(Paths.get("Arquivo2.txt")));
        BufferedOutputStream bos = new BufferedOutputStream(Files.newOutputStream(Paths.get("Arquivo2.txt")))) {
      byte[] byt = { 65, 66, 67, 68 };
      bos.write(byt);
      bos.flush();

      int a;
      while ((a = bis.read()) != -1) {
        byte b = (byte) a;
        System.out.println(b);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
